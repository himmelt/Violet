package org.soraworld.violet;


/**
 * Violet 常量.
 */
public final class Violet {
    public static final String PLUGIN_ID = "violet";
    public static final String ASSETS_ID = "violet";
    public static final String PLUGIN_NAME = "Violet";
    public static final String PLUGIN_VERSION = "2.3.2";
}
